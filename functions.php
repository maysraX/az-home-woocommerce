<?php
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; 

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

// adding a custom javascript depending on Jquery
function a_home_scripts_method() {
    wp_enqueue_script( 'custom-script', get_stylesheet_directory_uri() . '/assets/js/script.js', array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'a_home_scripts_method' );

// Custom woocommerce
function my_woocommerce_titles_list($atts) {
    extract(shortcode_atts(array(
        'number_of_posts'   => -1,
        'category'      => '',
        'orderby'       => 'title'
        ), $atts));

    $return_string = '<ul class="product-titles-list">'.PHP_EOL;

    global $post;
    $args = array(
        'posts_per_page'        => $number_of_posts,
        'offset'                => 0,
        'post_status'           => 'publish',
        'post_type'             => 'product',
        'product_cat'           => $category,
        'order'             => 'ASC',
        'orderby'           => $orderby
    );
    $postslist = get_posts( $args );
    foreach ( $postslist as $post ) :
        setup_postdata( $post );
        $my_permalink = get_the_permalink();
        $my_title = get_the_title();
        $return_string .= '<li><a href="' . $my_permalink . '">' . $my_title . '</a></li>'.PHP_EOL;

    endforeach;
    wp_reset_postdata();

    $return_string .= '</ul>'.PHP_EOL;

    return $return_string;

};

function my_register_shortcodes(){
   add_shortcode('my_product_titles', 'my_woocommerce_titles_list');
}

add_action( 'init', 'my_register_shortcodes');
// Woocommerce add field By: misraX
// Display Action
add_action('woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields');
// Save Fields
add_action('woocommerce_process_product_meta', 'woo_add_custom_general_fields_save');

// Add a text field
function woo_add_custom_general_fields()
{
    # code...
    global $woocommerce, $post;
    echo '<div class="options-group">';
    // Custom fields will be here
        woocommerce_wp_text_input
        (
            array(
                'id'          => '_text_field',
                'label'       => __('Code Field', 'woocommerce'),
                'placeholder' => 'Code #',
                'desc_tip'    => 'true',
                'description' => __('Enter product code.', 'woocommerce') 

                )
        );
    echo '</div>';
}
// assing the field
function woo_add_custom_general_fields_save( $post_id ){
    # code...
    $woocommerce_text_field = $_POST['_text_field'];
    if ( !empty( $woocommerce_text_field ) )
        # code...
 update_post_meta( $post_id, '_text_field', esc_attr( $woocommerce_text_field ) );
    
}
// Fixing shop title
add_filter( 'woocommerce_page_title', 'woo_shop_page_title');

function woo_shop_page_title( $page_title ) {
                      if( 'Shop' == $page_title) {
                                   return "Products";
                         }
            }
/*
 * wc_remove_related_products
 * 
 * Clear the query arguments for related products so none show.
 * Add this code to your theme functions.php file.  
 */
function wc_remove_related_products( $args ) {
    return array();
}
add_filter('woocommerce_related_products_args','wc_remove_related_products', 10); 
?>
